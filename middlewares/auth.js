const firebaseAdmin = require('../firebase/admin');

/**
 * Check if user has token
 */
exports.checkToken = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];

  if (!!token) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(decodedToken => next())
      .catch(err => {
        console.log(err);
        res.send(401);
      })
  } else {
    res.send(401);
  }
}

/**
 * Check if user has admin permissions
 */
exports.isAdmin = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];

  if (!!token) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(claims => {
        if (!!claims.admin) next();
        else res.send(401);
      })
  } else {
    res.send(401);
  }
}

/**
 * Check if user has place permissions
 */
exports.isPlace = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];

  if (!!token) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(claims => {
        if (!!claims.place) next();
        else res.send(401);
      })
  } else {
    res.send(401);
  }
}

/**
 * Check if user has player permissions
 */
exports.isPlayer = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];

  if (!!token) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(claims => {
        if (!!claims.player) next();
        else res.send(401);
      })
  } else {
    res.send(401);
  }
}

/**
 * Check if user has admin or place permissions
 */
exports.isAdminOrPlace = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];

  if (!!token) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(claims => {
        if (!!claims.admin || !!claims.place) next();
        else res.send(401);
      })
  } else {
    res.send(401);
  }
}

/**
 * Check if logged user is the same of the route
 */
exports.isOwner = (req, res, next) => {
  let token = req.get('Authorization').split(/\s+/g)[1];
  let uid = req.params.uid;

  if (!!token && !!uid) {
    firebaseAdmin.auth().verifyIdToken(token)
      .then(decodedToken => {
        if (!!decodedToken.uid === uid) next();
        else res.send(401);
      })
  } else {
    res.send(401);
  }
}