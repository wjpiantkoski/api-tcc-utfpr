const mongoose = require('mongoose');
const {CNPJ} = require('cpf_cnpj');

const Schema = mongoose.Schema;

const partnerSchema = new Schema({
  cnpj: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: (value) => {
        return CNPJ.isValid(value);
      },
      message: 'Invalid CNPJ'
    }
  },
  razaoSocial: {
    type: String,
    required: true,
    unique: true
  },
  displayName: String,
  uid: {
    type: String,
    unique: true
  },
  email: {
    type: String,
    unique: true
  }
});

module.exports = mongoose.model('Partner', partnerSchema);