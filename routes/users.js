const router = require('express').Router();
const admin = require('../firebase/admin');
const {checkToken} = require('../middlewares/auth');

router
  .route('/:uid')
  .put(checkToken, (req, res) => {
    console.log(new Date(), 'PUT /users/:uid');

    let uid = JSON.parse(JSON.stringify(req.body.uid));
    delete req.body.uid;
    let userData = JSON.parse(JSON.stringify(req.body));

    admin.auth().updateUser(uid, userData)
      .then(userRecord => res.json({ status: true }))
      .catch(err => {
        console.log(new Date(), 'POST /users/change-email', err);
        res.json({ status: false, err: err.errorInfo.code });
      });
  });

router
  .route('/check-email')
  .post((req, res) => {
    console.log(new Date(), 'POST /users/check-email');

    admin.auth().getUserByEmail(req.body.email)
      .then(storedUser => {
        if (storedUser.uid != req.body.uid) {
          res.json({ status: true, exists: true });
        } else {
          res,json({ status: true, exists: false });
        }
      })
      .catch(err => {
        if (err.errorInfo.code === 'auth/user-not-found') {
          res.json({ status: true, exists: false });
        } else {
          console.log(new Date(), err);
          res.json({ status: false, err: err.errorInfo.code });
        }
      });
  });
  

module.exports = router;