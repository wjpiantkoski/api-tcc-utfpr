const router = require('express').Router();
const Partner = require('../models/partner');
const {isAdmin, checkToken} = require('../middlewares/auth');

router
  .route('/')
  .get(isAdmin, (req, res) => {
    console.log(new Date(), 'GET /partners');

    Partner
      .find()
      .lean()
      .sort({ razaoSocial: 1 })
      .skip(parseInt(req.query.skip))
      .limit(parseInt(req.query.limit))
      .then(partners => res.json({ status: true, partners }))
      .catch(err => {
        console.log(new Date(), 'GET /partners', err);
        res.json({ status: false, err: err._message });
      });
  })
  .post(isAdmin, (req, res) => {
    console.log(new Date(), 'POST /partners');

    Partner.create(req.body)
      .then(partner => res.json({ status: true, partner }))
      .catch(err => {
        console.log(new Date(), 'POST /partners', err);
        res.json({ status: false, err: err._message });
      });
  })
  .delete(isAdmin, (req, res) => {
    console.log(new Date(), 'DELETE /partners');

    Partner.deleteMany({ _id: { $in: req.body } })
      .then(resp => res.json({ status: true }))
      .catch(err => {
        console.log(new Date(), 'DELETE /partners', err);
        res.json({ status: false, err: err._message });
      });
  })

router
  .route('/count')
  .get(isAdmin, (req, res) => {
    console.log(new Date(), 'GET /partners/count');

    Partner.countDocuments()
      .then(count => res.json({ status: true, count }))
      .catch(err => {
        console.log(new Date(), 'GET /partners/count', err);
        res.json({ status: false, err: err._message });
      });
  });

router
  .route('/check-cnpj')
  .post(isAdmin, (req, res) => {
    console.log(new Date(), 'POST /partners/check-cnpj');

    Partner.findOne({ cnpj: req.body.cnpj })
      .then(partner => {
        if (!partner || partner._id == req.body._id) {
          res.json({ status: true, exists: false });
        } else {
          res.json({ status: true, exists: true });
        }
      })
      .catch(err => {
        console.log(new Date(), 'POST /partners/check-cnpj', err);
        res.json({ status: false, err: err._message });
      });
  });

router
  .route('/:id')
  .put(checkToken, (req, res) => {
    console.log(new Date(), 'PUT /partners/:id');

    delete req.body._id;

    Partner.findByIdAndUpdate(req.params.id, req.body, { new: true })
      .then(partner => res.json({ status: true, partner }))
      .catch(err => {
        console.log(new Date(), 'PUT /partners/:id', err);
        res.json({ status: false, err: err._message });
      });
  });

router
  .route('/:cnpj')
  .get((req, res) => {
    console.log(new Date(), 'GET /partners/:cnpj');

    Partner.findOne({cnpj: req.params.cnpj})
      .then(partner => {
        if (!partner) {
          res.json({ status: false, err: 'Partner not found' });
        } else {
          res.json({ status: true, partner });
        }
      })
      .catch(err => {
        console.log(new Date(), 'GET /partners/:cnpj', err);
        res.json({ status: false, err: err._message });
      });
  })

module.exports = router;