const cors = require('cors');
const path = require('path');
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');

// database config
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect('mongodb://localhost:27017/tcc', { useNewUrlParser: true });

// express config
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'static')));

// serve static files
app.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname, 'static/index.html'), err => {
      if (err) 
        next(err);
      else 
        console.log('done');
  });
});

// routes
app.use('/users', require('./routes/users'));
app.use('/partners', require('./routes/partners'));

app.listen(3000, () => console.log('SERVER RUNNING AT http://localhost:3000'));